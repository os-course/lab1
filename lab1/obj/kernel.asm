
bin/kernel:     file format elf64-littleriscv


Disassembly of section .text:

0000000080200000 <kern_entry>:
#include <memlayout.h>

    .section .text,"ax",%progbits
    .globl kern_entry
kern_entry:
    la sp, bootstacktop
    80200000:	00004117          	auipc	sp,0x4
    80200004:	00010113          	mv	sp,sp

    tail kern_init
    80200008:	0040006f          	j	8020000c <kern_init>

000000008020000c <kern_init>:
int kern_init(void) __attribute__((noreturn));
void grade_backtrace(void);

int kern_init(void) {
    extern char edata[], end[];
    memset(edata, 0, end - edata);
    8020000c:	00004517          	auipc	a0,0x4
    80200010:	00450513          	addi	a0,a0,4 # 80204010 <edata>
    80200014:	00004617          	auipc	a2,0x4
    80200018:	01460613          	addi	a2,a2,20 # 80204028 <end>
int kern_init(void) {
    8020001c:	1141                	addi	sp,sp,-16
    memset(edata, 0, end - edata);
    8020001e:	8e09                	sub	a2,a2,a0
    80200020:	4581                	li	a1,0
int kern_init(void) {
    80200022:	e406                	sd	ra,8(sp)
    memset(edata, 0, end - edata);
    80200024:	221000ef          	jal	ra,80200a44 <memset>

    cons_init();  // init the console
    80200028:	150000ef          	jal	ra,80200178 <cons_init>

    const char *message = "(THU.CST) os is loading ...\n";
    cprintf("%s\n\n", message);
    8020002c:	00001597          	auipc	a1,0x1
    80200030:	a2c58593          	addi	a1,a1,-1492 # 80200a58 <etext+0x2>
    80200034:	00001517          	auipc	a0,0x1
    80200038:	a4450513          	addi	a0,a0,-1468 # 80200a78 <etext+0x22>
    8020003c:	034000ef          	jal	ra,80200070 <cprintf>

    print_kerninfo();
    80200040:	064000ef          	jal	ra,802000a4 <print_kerninfo>

    // grade_backtrace();

    idt_init();  // init interrupt descriptor table
    80200044:	144000ef          	jal	ra,80200188 <idt_init>

    // rdtime in mbare mode crashes
    clock_init();  // init clock interrupt
    80200048:	0ec000ef          	jal	ra,80200134 <clock_init>

    intr_enable();  // enable irq interrupt
    8020004c:	136000ef          	jal	ra,80200182 <intr_enable>

    __asm__("mret");
    80200050:	30200073          	mret
    
    while (1)
        ;
    80200054:	a001                	j	80200054 <kern_init+0x48>

0000000080200056 <cputch>:

/* *
 * cputch - writes a single character @c to stdout, and it will
 * increace the value of counter pointed by @cnt.
 * */
static void cputch(int c, int *cnt) {
    80200056:	1141                	addi	sp,sp,-16
    80200058:	e022                	sd	s0,0(sp)
    8020005a:	e406                	sd	ra,8(sp)
    8020005c:	842e                	mv	s0,a1
    cons_putc(c);
    8020005e:	11c000ef          	jal	ra,8020017a <cons_putc>
    (*cnt)++;
    80200062:	401c                	lw	a5,0(s0)
}
    80200064:	60a2                	ld	ra,8(sp)
    (*cnt)++;
    80200066:	2785                	addiw	a5,a5,1
    80200068:	c01c                	sw	a5,0(s0)
}
    8020006a:	6402                	ld	s0,0(sp)
    8020006c:	0141                	addi	sp,sp,16
    8020006e:	8082                	ret

0000000080200070 <cprintf>:
 * cprintf - formats a string and writes it to stdout
 *
 * The return value is the number of characters which would be
 * written to stdout.
 * */
int cprintf(const char *fmt, ...) {
    80200070:	711d                	addi	sp,sp,-96
    va_list ap;
    int cnt;
    va_start(ap, fmt);
    80200072:	02810313          	addi	t1,sp,40 # 80204028 <end>
int cprintf(const char *fmt, ...) {
    80200076:	f42e                	sd	a1,40(sp)
    80200078:	f832                	sd	a2,48(sp)
    8020007a:	fc36                	sd	a3,56(sp)
    vprintfmt((void *)cputch, &cnt, fmt, ap);
    8020007c:	862a                	mv	a2,a0
    8020007e:	004c                	addi	a1,sp,4
    80200080:	00000517          	auipc	a0,0x0
    80200084:	fd650513          	addi	a0,a0,-42 # 80200056 <cputch>
    80200088:	869a                	mv	a3,t1
int cprintf(const char *fmt, ...) {
    8020008a:	ec06                	sd	ra,24(sp)
    8020008c:	e0ba                	sd	a4,64(sp)
    8020008e:	e4be                	sd	a5,72(sp)
    80200090:	e8c2                	sd	a6,80(sp)
    80200092:	ecc6                	sd	a7,88(sp)
    va_start(ap, fmt);
    80200094:	e41a                	sd	t1,8(sp)
    int cnt = 0;
    80200096:	c202                	sw	zero,4(sp)
    vprintfmt((void *)cputch, &cnt, fmt, ap);
    80200098:	5a6000ef          	jal	ra,8020063e <vprintfmt>
    cnt = vcprintf(fmt, ap);
    va_end(ap);
    return cnt;
}
    8020009c:	60e2                	ld	ra,24(sp)
    8020009e:	4512                	lw	a0,4(sp)
    802000a0:	6125                	addi	sp,sp,96
    802000a2:	8082                	ret

00000000802000a4 <print_kerninfo>:
/* *
 * print_kerninfo - print the information about kernel, including the location
 * of kernel entry, the start addresses of data and text segements, the start
 * address of free memory and how many memory that kernel has used.
 * */
void print_kerninfo(void) {
    802000a4:	1141                	addi	sp,sp,-16
    extern char etext[], edata[], end[], kern_init[];
    cprintf("Special kernel symbols:\n");
    802000a6:	00001517          	auipc	a0,0x1
    802000aa:	9da50513          	addi	a0,a0,-1574 # 80200a80 <etext+0x2a>
void print_kerninfo(void) {
    802000ae:	e406                	sd	ra,8(sp)
    cprintf("Special kernel symbols:\n");
    802000b0:	fc1ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  entry  0x%016x (virtual)\n", kern_init);
    802000b4:	00000597          	auipc	a1,0x0
    802000b8:	f5858593          	addi	a1,a1,-168 # 8020000c <kern_init>
    802000bc:	00001517          	auipc	a0,0x1
    802000c0:	9e450513          	addi	a0,a0,-1564 # 80200aa0 <etext+0x4a>
    802000c4:	fadff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  etext  0x%016x (virtual)\n", etext);
    802000c8:	00001597          	auipc	a1,0x1
    802000cc:	98e58593          	addi	a1,a1,-1650 # 80200a56 <etext>
    802000d0:	00001517          	auipc	a0,0x1
    802000d4:	9f050513          	addi	a0,a0,-1552 # 80200ac0 <etext+0x6a>
    802000d8:	f99ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  edata  0x%016x (virtual)\n", edata);
    802000dc:	00004597          	auipc	a1,0x4
    802000e0:	f3458593          	addi	a1,a1,-204 # 80204010 <edata>
    802000e4:	00001517          	auipc	a0,0x1
    802000e8:	9fc50513          	addi	a0,a0,-1540 # 80200ae0 <etext+0x8a>
    802000ec:	f85ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  end    0x%016x (virtual)\n", end);
    802000f0:	00004597          	auipc	a1,0x4
    802000f4:	f3858593          	addi	a1,a1,-200 # 80204028 <end>
    802000f8:	00001517          	auipc	a0,0x1
    802000fc:	a0850513          	addi	a0,a0,-1528 # 80200b00 <etext+0xaa>
    80200100:	f71ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("Kernel executable memory footprint: %dKB\n",
            (end - kern_init + 1023) / 1024);
    80200104:	00004597          	auipc	a1,0x4
    80200108:	32358593          	addi	a1,a1,803 # 80204427 <end+0x3ff>
    8020010c:	00000797          	auipc	a5,0x0
    80200110:	f0078793          	addi	a5,a5,-256 # 8020000c <kern_init>
    80200114:	40f587b3          	sub	a5,a1,a5
    cprintf("Kernel executable memory footprint: %dKB\n",
    80200118:	43f7d593          	srai	a1,a5,0x3f
}
    8020011c:	60a2                	ld	ra,8(sp)
    cprintf("Kernel executable memory footprint: %dKB\n",
    8020011e:	3ff5f593          	andi	a1,a1,1023
    80200122:	95be                	add	a1,a1,a5
    80200124:	85a9                	srai	a1,a1,0xa
    80200126:	00001517          	auipc	a0,0x1
    8020012a:	9fa50513          	addi	a0,a0,-1542 # 80200b20 <etext+0xca>
}
    8020012e:	0141                	addi	sp,sp,16
    cprintf("Kernel executable memory footprint: %dKB\n",
    80200130:	f41ff06f          	j	80200070 <cprintf>

0000000080200134 <clock_init>:

/* *
 * clock_init - initialize 8253 clock to interrupt 100 times per second,
 * and then enable IRQ_TIMER.
 * */
void clock_init(void) {
    80200134:	1141                	addi	sp,sp,-16
    80200136:	e406                	sd	ra,8(sp)
    // enable timer interrupt in sie
    set_csr(sie, MIP_STIP);
    80200138:	02000793          	li	a5,32
    8020013c:	1047a7f3          	csrrs	a5,sie,a5
    __asm__ __volatile__("rdtime %0" : "=r"(n));
    80200140:	c0102573          	rdtime	a0
    ticks = 0;

    cprintf("++ setup timer interrupts\n");
}

void clock_set_next_event(void) { sbi_set_timer(get_cycles() + timebase); }
    80200144:	67e1                	lui	a5,0x18
    80200146:	6a078793          	addi	a5,a5,1696 # 186a0 <BASE_ADDRESS-0x801e7960>
    8020014a:	953e                	add	a0,a0,a5
    8020014c:	09b000ef          	jal	ra,802009e6 <sbi_set_timer>
}
    80200150:	60a2                	ld	ra,8(sp)
    ticks = 0;
    80200152:	00004797          	auipc	a5,0x4
    80200156:	ec07b723          	sd	zero,-306(a5) # 80204020 <ticks>
    cprintf("++ setup timer interrupts\n");
    8020015a:	00001517          	auipc	a0,0x1
    8020015e:	9f650513          	addi	a0,a0,-1546 # 80200b50 <etext+0xfa>
}
    80200162:	0141                	addi	sp,sp,16
    cprintf("++ setup timer interrupts\n");
    80200164:	f0dff06f          	j	80200070 <cprintf>

0000000080200168 <clock_set_next_event>:
    __asm__ __volatile__("rdtime %0" : "=r"(n));
    80200168:	c0102573          	rdtime	a0
void clock_set_next_event(void) { sbi_set_timer(get_cycles() + timebase); }
    8020016c:	67e1                	lui	a5,0x18
    8020016e:	6a078793          	addi	a5,a5,1696 # 186a0 <BASE_ADDRESS-0x801e7960>
    80200172:	953e                	add	a0,a0,a5
    80200174:	0730006f          	j	802009e6 <sbi_set_timer>

0000000080200178 <cons_init>:

/* serial_intr - try to feed input characters from serial port */
void serial_intr(void) {}

/* cons_init - initializes the console devices */
void cons_init(void) {}
    80200178:	8082                	ret

000000008020017a <cons_putc>:

/* cons_putc - print a single character @c to console devices */
void cons_putc(int c) { sbi_console_putchar((unsigned char)c); }
    8020017a:	0ff57513          	andi	a0,a0,255
    8020017e:	04d0006f          	j	802009ca <sbi_console_putchar>

0000000080200182 <intr_enable>:
#include <intr.h>
#include <riscv.h>

/* intr_enable - enable irq interrupt */
void intr_enable(void) { set_csr(sstatus, SSTATUS_SIE); }
    80200182:	100167f3          	csrrsi	a5,sstatus,2
    80200186:	8082                	ret

0000000080200188 <idt_init>:
 */
void idt_init(void) {
    extern void __alltraps(void);
    /* Set sscratch register to 0, indicating to exception vector that we are
     * presently executing in the kernel */
    write_csr(sscratch, 0);
    80200188:	14005073          	csrwi	sscratch,0
    /* Set the exception vector address */
    write_csr(stvec, &__alltraps);
    8020018c:	00000797          	auipc	a5,0x0
    80200190:	39078793          	addi	a5,a5,912 # 8020051c <__alltraps>
    80200194:	10579073          	csrw	stvec,a5
}
    80200198:	8082                	ret

000000008020019a <print_regs>:
    cprintf("  badvaddr 0x%08x\n", tf->badvaddr);
    cprintf("  cause    0x%08x\n", tf->cause);
}

void print_regs(struct pushregs *gpr) {
    cprintf("  zero     0x%08x\n", gpr->zero);
    8020019a:	610c                	ld	a1,0(a0)
void print_regs(struct pushregs *gpr) {
    8020019c:	1141                	addi	sp,sp,-16
    8020019e:	e022                	sd	s0,0(sp)
    802001a0:	842a                	mv	s0,a0
    cprintf("  zero     0x%08x\n", gpr->zero);
    802001a2:	00001517          	auipc	a0,0x1
    802001a6:	b2e50513          	addi	a0,a0,-1234 # 80200cd0 <etext+0x27a>
void print_regs(struct pushregs *gpr) {
    802001aa:	e406                	sd	ra,8(sp)
    cprintf("  zero     0x%08x\n", gpr->zero);
    802001ac:	ec5ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  ra       0x%08x\n", gpr->ra);
    802001b0:	640c                	ld	a1,8(s0)
    802001b2:	00001517          	auipc	a0,0x1
    802001b6:	b3650513          	addi	a0,a0,-1226 # 80200ce8 <etext+0x292>
    802001ba:	eb7ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  sp       0x%08x\n", gpr->sp);
    802001be:	680c                	ld	a1,16(s0)
    802001c0:	00001517          	auipc	a0,0x1
    802001c4:	b4050513          	addi	a0,a0,-1216 # 80200d00 <etext+0x2aa>
    802001c8:	ea9ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  gp       0x%08x\n", gpr->gp);
    802001cc:	6c0c                	ld	a1,24(s0)
    802001ce:	00001517          	auipc	a0,0x1
    802001d2:	b4a50513          	addi	a0,a0,-1206 # 80200d18 <etext+0x2c2>
    802001d6:	e9bff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  tp       0x%08x\n", gpr->tp);
    802001da:	700c                	ld	a1,32(s0)
    802001dc:	00001517          	auipc	a0,0x1
    802001e0:	b5450513          	addi	a0,a0,-1196 # 80200d30 <etext+0x2da>
    802001e4:	e8dff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t0       0x%08x\n", gpr->t0);
    802001e8:	740c                	ld	a1,40(s0)
    802001ea:	00001517          	auipc	a0,0x1
    802001ee:	b5e50513          	addi	a0,a0,-1186 # 80200d48 <etext+0x2f2>
    802001f2:	e7fff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t1       0x%08x\n", gpr->t1);
    802001f6:	780c                	ld	a1,48(s0)
    802001f8:	00001517          	auipc	a0,0x1
    802001fc:	b6850513          	addi	a0,a0,-1176 # 80200d60 <etext+0x30a>
    80200200:	e71ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t2       0x%08x\n", gpr->t2);
    80200204:	7c0c                	ld	a1,56(s0)
    80200206:	00001517          	auipc	a0,0x1
    8020020a:	b7250513          	addi	a0,a0,-1166 # 80200d78 <etext+0x322>
    8020020e:	e63ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s0       0x%08x\n", gpr->s0);
    80200212:	602c                	ld	a1,64(s0)
    80200214:	00001517          	auipc	a0,0x1
    80200218:	b7c50513          	addi	a0,a0,-1156 # 80200d90 <etext+0x33a>
    8020021c:	e55ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s1       0x%08x\n", gpr->s1);
    80200220:	642c                	ld	a1,72(s0)
    80200222:	00001517          	auipc	a0,0x1
    80200226:	b8650513          	addi	a0,a0,-1146 # 80200da8 <etext+0x352>
    8020022a:	e47ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a0       0x%08x\n", gpr->a0);
    8020022e:	682c                	ld	a1,80(s0)
    80200230:	00001517          	auipc	a0,0x1
    80200234:	b9050513          	addi	a0,a0,-1136 # 80200dc0 <etext+0x36a>
    80200238:	e39ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a1       0x%08x\n", gpr->a1);
    8020023c:	6c2c                	ld	a1,88(s0)
    8020023e:	00001517          	auipc	a0,0x1
    80200242:	b9a50513          	addi	a0,a0,-1126 # 80200dd8 <etext+0x382>
    80200246:	e2bff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a2       0x%08x\n", gpr->a2);
    8020024a:	702c                	ld	a1,96(s0)
    8020024c:	00001517          	auipc	a0,0x1
    80200250:	ba450513          	addi	a0,a0,-1116 # 80200df0 <etext+0x39a>
    80200254:	e1dff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a3       0x%08x\n", gpr->a3);
    80200258:	742c                	ld	a1,104(s0)
    8020025a:	00001517          	auipc	a0,0x1
    8020025e:	bae50513          	addi	a0,a0,-1106 # 80200e08 <etext+0x3b2>
    80200262:	e0fff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a4       0x%08x\n", gpr->a4);
    80200266:	782c                	ld	a1,112(s0)
    80200268:	00001517          	auipc	a0,0x1
    8020026c:	bb850513          	addi	a0,a0,-1096 # 80200e20 <etext+0x3ca>
    80200270:	e01ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a5       0x%08x\n", gpr->a5);
    80200274:	7c2c                	ld	a1,120(s0)
    80200276:	00001517          	auipc	a0,0x1
    8020027a:	bc250513          	addi	a0,a0,-1086 # 80200e38 <etext+0x3e2>
    8020027e:	df3ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a6       0x%08x\n", gpr->a6);
    80200282:	604c                	ld	a1,128(s0)
    80200284:	00001517          	auipc	a0,0x1
    80200288:	bcc50513          	addi	a0,a0,-1076 # 80200e50 <etext+0x3fa>
    8020028c:	de5ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  a7       0x%08x\n", gpr->a7);
    80200290:	644c                	ld	a1,136(s0)
    80200292:	00001517          	auipc	a0,0x1
    80200296:	bd650513          	addi	a0,a0,-1066 # 80200e68 <etext+0x412>
    8020029a:	dd7ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s2       0x%08x\n", gpr->s2);
    8020029e:	684c                	ld	a1,144(s0)
    802002a0:	00001517          	auipc	a0,0x1
    802002a4:	be050513          	addi	a0,a0,-1056 # 80200e80 <etext+0x42a>
    802002a8:	dc9ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s3       0x%08x\n", gpr->s3);
    802002ac:	6c4c                	ld	a1,152(s0)
    802002ae:	00001517          	auipc	a0,0x1
    802002b2:	bea50513          	addi	a0,a0,-1046 # 80200e98 <etext+0x442>
    802002b6:	dbbff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s4       0x%08x\n", gpr->s4);
    802002ba:	704c                	ld	a1,160(s0)
    802002bc:	00001517          	auipc	a0,0x1
    802002c0:	bf450513          	addi	a0,a0,-1036 # 80200eb0 <etext+0x45a>
    802002c4:	dadff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s5       0x%08x\n", gpr->s5);
    802002c8:	744c                	ld	a1,168(s0)
    802002ca:	00001517          	auipc	a0,0x1
    802002ce:	bfe50513          	addi	a0,a0,-1026 # 80200ec8 <etext+0x472>
    802002d2:	d9fff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s6       0x%08x\n", gpr->s6);
    802002d6:	784c                	ld	a1,176(s0)
    802002d8:	00001517          	auipc	a0,0x1
    802002dc:	c0850513          	addi	a0,a0,-1016 # 80200ee0 <etext+0x48a>
    802002e0:	d91ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s7       0x%08x\n", gpr->s7);
    802002e4:	7c4c                	ld	a1,184(s0)
    802002e6:	00001517          	auipc	a0,0x1
    802002ea:	c1250513          	addi	a0,a0,-1006 # 80200ef8 <etext+0x4a2>
    802002ee:	d83ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s8       0x%08x\n", gpr->s8);
    802002f2:	606c                	ld	a1,192(s0)
    802002f4:	00001517          	auipc	a0,0x1
    802002f8:	c1c50513          	addi	a0,a0,-996 # 80200f10 <etext+0x4ba>
    802002fc:	d75ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s9       0x%08x\n", gpr->s9);
    80200300:	646c                	ld	a1,200(s0)
    80200302:	00001517          	auipc	a0,0x1
    80200306:	c2650513          	addi	a0,a0,-986 # 80200f28 <etext+0x4d2>
    8020030a:	d67ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s10      0x%08x\n", gpr->s10);
    8020030e:	686c                	ld	a1,208(s0)
    80200310:	00001517          	auipc	a0,0x1
    80200314:	c3050513          	addi	a0,a0,-976 # 80200f40 <etext+0x4ea>
    80200318:	d59ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  s11      0x%08x\n", gpr->s11);
    8020031c:	6c6c                	ld	a1,216(s0)
    8020031e:	00001517          	auipc	a0,0x1
    80200322:	c3a50513          	addi	a0,a0,-966 # 80200f58 <etext+0x502>
    80200326:	d4bff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t3       0x%08x\n", gpr->t3);
    8020032a:	706c                	ld	a1,224(s0)
    8020032c:	00001517          	auipc	a0,0x1
    80200330:	c4450513          	addi	a0,a0,-956 # 80200f70 <etext+0x51a>
    80200334:	d3dff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t4       0x%08x\n", gpr->t4);
    80200338:	746c                	ld	a1,232(s0)
    8020033a:	00001517          	auipc	a0,0x1
    8020033e:	c4e50513          	addi	a0,a0,-946 # 80200f88 <etext+0x532>
    80200342:	d2fff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t5       0x%08x\n", gpr->t5);
    80200346:	786c                	ld	a1,240(s0)
    80200348:	00001517          	auipc	a0,0x1
    8020034c:	c5850513          	addi	a0,a0,-936 # 80200fa0 <etext+0x54a>
    80200350:	d21ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  t6       0x%08x\n", gpr->t6);
    80200354:	7c6c                	ld	a1,248(s0)
}
    80200356:	6402                	ld	s0,0(sp)
    80200358:	60a2                	ld	ra,8(sp)
    cprintf("  t6       0x%08x\n", gpr->t6);
    8020035a:	00001517          	auipc	a0,0x1
    8020035e:	c5e50513          	addi	a0,a0,-930 # 80200fb8 <etext+0x562>
}
    80200362:	0141                	addi	sp,sp,16
    cprintf("  t6       0x%08x\n", gpr->t6);
    80200364:	d0dff06f          	j	80200070 <cprintf>

0000000080200368 <print_trapframe>:
void print_trapframe(struct trapframe *tf) {
    80200368:	1141                	addi	sp,sp,-16
    8020036a:	e022                	sd	s0,0(sp)
    cprintf("trapframe at %p\n", tf);
    8020036c:	85aa                	mv	a1,a0
void print_trapframe(struct trapframe *tf) {
    8020036e:	842a                	mv	s0,a0
    cprintf("trapframe at %p\n", tf);
    80200370:	00001517          	auipc	a0,0x1
    80200374:	c6050513          	addi	a0,a0,-928 # 80200fd0 <etext+0x57a>
void print_trapframe(struct trapframe *tf) {
    80200378:	e406                	sd	ra,8(sp)
    cprintf("trapframe at %p\n", tf);
    8020037a:	cf7ff0ef          	jal	ra,80200070 <cprintf>
    print_regs(&tf->gpr);
    8020037e:	8522                	mv	a0,s0
    80200380:	e1bff0ef          	jal	ra,8020019a <print_regs>
    cprintf("  status   0x%08x\n", tf->status);
    80200384:	10043583          	ld	a1,256(s0)
    80200388:	00001517          	auipc	a0,0x1
    8020038c:	c6050513          	addi	a0,a0,-928 # 80200fe8 <etext+0x592>
    80200390:	ce1ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  epc      0x%08x\n", tf->epc);
    80200394:	10843583          	ld	a1,264(s0)
    80200398:	00001517          	auipc	a0,0x1
    8020039c:	c6850513          	addi	a0,a0,-920 # 80201000 <etext+0x5aa>
    802003a0:	cd1ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  badvaddr 0x%08x\n", tf->badvaddr);
    802003a4:	11043583          	ld	a1,272(s0)
    802003a8:	00001517          	auipc	a0,0x1
    802003ac:	c7050513          	addi	a0,a0,-912 # 80201018 <etext+0x5c2>
    802003b0:	cc1ff0ef          	jal	ra,80200070 <cprintf>
    cprintf("  cause    0x%08x\n", tf->cause);
    802003b4:	11843583          	ld	a1,280(s0)
}
    802003b8:	6402                	ld	s0,0(sp)
    802003ba:	60a2                	ld	ra,8(sp)
    cprintf("  cause    0x%08x\n", tf->cause);
    802003bc:	00001517          	auipc	a0,0x1
    802003c0:	c7450513          	addi	a0,a0,-908 # 80201030 <etext+0x5da>
}
    802003c4:	0141                	addi	sp,sp,16
    cprintf("  cause    0x%08x\n", tf->cause);
    802003c6:	cabff06f          	j	80200070 <cprintf>

00000000802003ca <interrupt_handler>:

void interrupt_handler(struct trapframe *tf) {
    intptr_t cause = (tf->cause << 1) >> 1;
    802003ca:	11853783          	ld	a5,280(a0)
    802003ce:	577d                	li	a4,-1
    802003d0:	8305                	srli	a4,a4,0x1
    802003d2:	8ff9                	and	a5,a5,a4
    switch (cause) {
    802003d4:	472d                	li	a4,11
    802003d6:	06f76f63          	bltu	a4,a5,80200454 <interrupt_handler+0x8a>
    802003da:	00000717          	auipc	a4,0x0
    802003de:	79270713          	addi	a4,a4,1938 # 80200b6c <etext+0x116>
    802003e2:	078a                	slli	a5,a5,0x2
    802003e4:	97ba                	add	a5,a5,a4
    802003e6:	439c                	lw	a5,0(a5)
    802003e8:	97ba                	add	a5,a5,a4
    802003ea:	8782                	jr	a5
            break;
        case IRQ_H_SOFT:
            cprintf("Hypervisor software interrupt\n");
            break;
        case IRQ_M_SOFT:
            cprintf("Machine software interrupt\n");
    802003ec:	00001517          	auipc	a0,0x1
    802003f0:	87c50513          	addi	a0,a0,-1924 # 80200c68 <etext+0x212>
    802003f4:	c7dff06f          	j	80200070 <cprintf>
            cprintf("Hypervisor software interrupt\n");
    802003f8:	00001517          	auipc	a0,0x1
    802003fc:	85050513          	addi	a0,a0,-1968 # 80200c48 <etext+0x1f2>
    80200400:	c71ff06f          	j	80200070 <cprintf>
            cprintf("User software interrupt\n");
    80200404:	00001517          	auipc	a0,0x1
    80200408:	80450513          	addi	a0,a0,-2044 # 80200c08 <etext+0x1b2>
    8020040c:	c65ff06f          	j	80200070 <cprintf>
            cprintf("Supervisor software interrupt\n");
    80200410:	00001517          	auipc	a0,0x1
    80200414:	81850513          	addi	a0,a0,-2024 # 80200c28 <etext+0x1d2>
    80200418:	c59ff06f          	j	80200070 <cprintf>
            break;
        case IRQ_U_EXT:
            cprintf("User software interrupt\n");
            break;
        case IRQ_S_EXT:
            cprintf("Supervisor external interrupt\n");
    8020041c:	00001517          	auipc	a0,0x1
    80200420:	89450513          	addi	a0,a0,-1900 # 80200cb0 <etext+0x25a>
    80200424:	c4dff06f          	j	80200070 <cprintf>
void interrupt_handler(struct trapframe *tf) {
    80200428:	1141                	addi	sp,sp,-16
    8020042a:	e406                	sd	ra,8(sp)
            clock_set_next_event();//发生这次时钟中断的时候，我们要设置下一次时钟中断
    8020042c:	d3dff0ef          	jal	ra,80200168 <clock_set_next_event>
            if (++ticks % TICK_NUM == 0) {
    80200430:	00004797          	auipc	a5,0x4
    80200434:	bf078793          	addi	a5,a5,-1040 # 80204020 <ticks>
    80200438:	639c                	ld	a5,0(a5)
    8020043a:	06400713          	li	a4,100
    8020043e:	0785                	addi	a5,a5,1
    80200440:	02e7f733          	remu	a4,a5,a4
    80200444:	00004697          	auipc	a3,0x4
    80200448:	bcf6be23          	sd	a5,-1060(a3) # 80204020 <ticks>
    8020044c:	c711                	beqz	a4,80200458 <interrupt_handler+0x8e>
            break;
        default:
            print_trapframe(tf);
            break;
    }
}
    8020044e:	60a2                	ld	ra,8(sp)
    80200450:	0141                	addi	sp,sp,16
    80200452:	8082                	ret
            print_trapframe(tf);
    80200454:	f15ff06f          	j	80200368 <print_trapframe>
    cprintf("%d ticks\n", TICK_NUM);
    80200458:	06400593          	li	a1,100
    8020045c:	00001517          	auipc	a0,0x1
    80200460:	82c50513          	addi	a0,a0,-2004 # 80200c88 <etext+0x232>
    80200464:	c0dff0ef          	jal	ra,80200070 <cprintf>
    asm volatile("ebreak");
    80200468:	9002                	ebreak
                printCount++;
    8020046a:	00004797          	auipc	a5,0x4
    8020046e:	ba678793          	addi	a5,a5,-1114 # 80204010 <edata>
    80200472:	439c                	lw	a5,0(a5)
                if(printCount == 10)
    80200474:	4729                	li	a4,10
                printCount++;
    80200476:	0017869b          	addiw	a3,a5,1
    8020047a:	00004617          	auipc	a2,0x4
    8020047e:	b8d62b23          	sw	a3,-1130(a2) # 80204010 <edata>
                if(printCount == 10)
    80200482:	fce696e3          	bne	a3,a4,8020044e <interrupt_handler+0x84>
                    cprintf("printCount = 10\n");
    80200486:	00001517          	auipc	a0,0x1
    8020048a:	81250513          	addi	a0,a0,-2030 # 80200c98 <etext+0x242>
    8020048e:	be3ff0ef          	jal	ra,80200070 <cprintf>
}
    80200492:	60a2                	ld	ra,8(sp)
    80200494:	0141                	addi	sp,sp,16
                    sbi_shutdown();
    80200496:	56c0006f          	j	80200a02 <sbi_shutdown>

000000008020049a <exception_handler>:

void exception_handler(struct trapframe *tf) {
    switch (tf->cause) {
    8020049a:	11853783          	ld	a5,280(a0)
    8020049e:	472d                	li	a4,11
    802004a0:	02f76863          	bltu	a4,a5,802004d0 <exception_handler+0x36>
    802004a4:	4705                	li	a4,1
    802004a6:	00f71733          	sll	a4,a4,a5
    802004aa:	6785                	lui	a5,0x1
    802004ac:	17cd                	addi	a5,a5,-13
    802004ae:	8ff9                	and	a5,a5,a4
    802004b0:	ef99                	bnez	a5,802004ce <exception_handler+0x34>
void exception_handler(struct trapframe *tf) {
    802004b2:	1141                	addi	sp,sp,-16
    802004b4:	e022                	sd	s0,0(sp)
    802004b6:	e406                	sd	ra,8(sp)
    802004b8:	00877793          	andi	a5,a4,8
    802004bc:	842a                	mv	s0,a0
    802004be:	e3b1                	bnez	a5,80200502 <exception_handler+0x68>
    802004c0:	8b11                	andi	a4,a4,4
    802004c2:	eb09                	bnez	a4,802004d4 <exception_handler+0x3a>
            break;
        default:
            print_trapframe(tf);
            break;
    }
}
    802004c4:	6402                	ld	s0,0(sp)
    802004c6:	60a2                	ld	ra,8(sp)
    802004c8:	0141                	addi	sp,sp,16
            print_trapframe(tf);
    802004ca:	e9fff06f          	j	80200368 <print_trapframe>
    802004ce:	8082                	ret
    802004d0:	e99ff06f          	j	80200368 <print_trapframe>
            cprintf("Exception type: Illegal instruction\n");
    802004d4:	00000517          	auipc	a0,0x0
    802004d8:	6cc50513          	addi	a0,a0,1740 # 80200ba0 <etext+0x14a>
            cprintf("Exception type: breakpoint\n");
    802004dc:	b95ff0ef          	jal	ra,80200070 <cprintf>
            cprintf("ebreak caught at 0x%016llx\n", tf->epc);
    802004e0:	10843583          	ld	a1,264(s0)
    802004e4:	00000517          	auipc	a0,0x0
    802004e8:	6e450513          	addi	a0,a0,1764 # 80200bc8 <etext+0x172>
    802004ec:	b85ff0ef          	jal	ra,80200070 <cprintf>
            tf->epc += 2;
    802004f0:	10843783          	ld	a5,264(s0)
}
    802004f4:	60a2                	ld	ra,8(sp)
            tf->epc += 2;
    802004f6:	0789                	addi	a5,a5,2
    802004f8:	10f43423          	sd	a5,264(s0)
}
    802004fc:	6402                	ld	s0,0(sp)
    802004fe:	0141                	addi	sp,sp,16
    80200500:	8082                	ret
            cprintf("Exception type: breakpoint\n");
    80200502:	00000517          	auipc	a0,0x0
    80200506:	6e650513          	addi	a0,a0,1766 # 80200be8 <etext+0x192>
    8020050a:	bfc9                	j	802004dc <exception_handler+0x42>

000000008020050c <trap>:

/* trap_dispatch - dispatch based on what type of trap occurred */
static inline void trap_dispatch(struct trapframe *tf) {
    if ((intptr_t)tf->cause < 0) {
    8020050c:	11853783          	ld	a5,280(a0)
    80200510:	0007c463          	bltz	a5,80200518 <trap+0xc>
        // interrupts
        interrupt_handler(tf);
    } else {
        // exceptions
        exception_handler(tf);
    80200514:	f87ff06f          	j	8020049a <exception_handler>
        interrupt_handler(tf);
    80200518:	eb3ff06f          	j	802003ca <interrupt_handler>

000000008020051c <__alltraps>:
    .endm

    .globl __alltraps
.align(2)
__alltraps:
    SAVE_ALL
    8020051c:	14011073          	csrw	sscratch,sp
    80200520:	712d                	addi	sp,sp,-288
    80200522:	e002                	sd	zero,0(sp)
    80200524:	e406                	sd	ra,8(sp)
    80200526:	ec0e                	sd	gp,24(sp)
    80200528:	f012                	sd	tp,32(sp)
    8020052a:	f416                	sd	t0,40(sp)
    8020052c:	f81a                	sd	t1,48(sp)
    8020052e:	fc1e                	sd	t2,56(sp)
    80200530:	e0a2                	sd	s0,64(sp)
    80200532:	e4a6                	sd	s1,72(sp)
    80200534:	e8aa                	sd	a0,80(sp)
    80200536:	ecae                	sd	a1,88(sp)
    80200538:	f0b2                	sd	a2,96(sp)
    8020053a:	f4b6                	sd	a3,104(sp)
    8020053c:	f8ba                	sd	a4,112(sp)
    8020053e:	fcbe                	sd	a5,120(sp)
    80200540:	e142                	sd	a6,128(sp)
    80200542:	e546                	sd	a7,136(sp)
    80200544:	e94a                	sd	s2,144(sp)
    80200546:	ed4e                	sd	s3,152(sp)
    80200548:	f152                	sd	s4,160(sp)
    8020054a:	f556                	sd	s5,168(sp)
    8020054c:	f95a                	sd	s6,176(sp)
    8020054e:	fd5e                	sd	s7,184(sp)
    80200550:	e1e2                	sd	s8,192(sp)
    80200552:	e5e6                	sd	s9,200(sp)
    80200554:	e9ea                	sd	s10,208(sp)
    80200556:	edee                	sd	s11,216(sp)
    80200558:	f1f2                	sd	t3,224(sp)
    8020055a:	f5f6                	sd	t4,232(sp)
    8020055c:	f9fa                	sd	t5,240(sp)
    8020055e:	fdfe                	sd	t6,248(sp)
    80200560:	14001473          	csrrw	s0,sscratch,zero
    80200564:	100024f3          	csrr	s1,sstatus
    80200568:	14102973          	csrr	s2,sepc
    8020056c:	143029f3          	csrr	s3,stval
    80200570:	14202a73          	csrr	s4,scause
    80200574:	e822                	sd	s0,16(sp)
    80200576:	e226                	sd	s1,256(sp)
    80200578:	e64a                	sd	s2,264(sp)
    8020057a:	ea4e                	sd	s3,272(sp)
    8020057c:	ee52                	sd	s4,280(sp)

    move  a0, sp
    8020057e:	850a                	mv	a0,sp
    jal trap
    80200580:	f8dff0ef          	jal	ra,8020050c <trap>

0000000080200584 <__trapret>:
    # sp should be the same as before "jal trap"

    .globl __trapret
__trapret:
    RESTORE_ALL
    80200584:	6492                	ld	s1,256(sp)
    80200586:	6932                	ld	s2,264(sp)
    80200588:	10049073          	csrw	sstatus,s1
    8020058c:	14191073          	csrw	sepc,s2
    80200590:	60a2                	ld	ra,8(sp)
    80200592:	61e2                	ld	gp,24(sp)
    80200594:	7202                	ld	tp,32(sp)
    80200596:	72a2                	ld	t0,40(sp)
    80200598:	7342                	ld	t1,48(sp)
    8020059a:	73e2                	ld	t2,56(sp)
    8020059c:	6406                	ld	s0,64(sp)
    8020059e:	64a6                	ld	s1,72(sp)
    802005a0:	6546                	ld	a0,80(sp)
    802005a2:	65e6                	ld	a1,88(sp)
    802005a4:	7606                	ld	a2,96(sp)
    802005a6:	76a6                	ld	a3,104(sp)
    802005a8:	7746                	ld	a4,112(sp)
    802005aa:	77e6                	ld	a5,120(sp)
    802005ac:	680a                	ld	a6,128(sp)
    802005ae:	68aa                	ld	a7,136(sp)
    802005b0:	694a                	ld	s2,144(sp)
    802005b2:	69ea                	ld	s3,152(sp)
    802005b4:	7a0a                	ld	s4,160(sp)
    802005b6:	7aaa                	ld	s5,168(sp)
    802005b8:	7b4a                	ld	s6,176(sp)
    802005ba:	7bea                	ld	s7,184(sp)
    802005bc:	6c0e                	ld	s8,192(sp)
    802005be:	6cae                	ld	s9,200(sp)
    802005c0:	6d4e                	ld	s10,208(sp)
    802005c2:	6dee                	ld	s11,216(sp)
    802005c4:	7e0e                	ld	t3,224(sp)
    802005c6:	7eae                	ld	t4,232(sp)
    802005c8:	7f4e                	ld	t5,240(sp)
    802005ca:	7fee                	ld	t6,248(sp)
    802005cc:	6142                	ld	sp,16(sp)
    # return from supervisor call
    sret
    802005ce:	10200073          	sret

00000000802005d2 <printnum>:
 * */
static void
printnum(void (*putch)(int, void*), void *putdat,
        unsigned long long num, unsigned base, int width, int padc) {
    unsigned long long result = num;
    unsigned mod = do_div(result, base);
    802005d2:	02069813          	slli	a6,a3,0x20
        unsigned long long num, unsigned base, int width, int padc) {
    802005d6:	7179                	addi	sp,sp,-48
    unsigned mod = do_div(result, base);
    802005d8:	02085813          	srli	a6,a6,0x20
        unsigned long long num, unsigned base, int width, int padc) {
    802005dc:	e052                	sd	s4,0(sp)
    unsigned mod = do_div(result, base);
    802005de:	03067a33          	remu	s4,a2,a6
        unsigned long long num, unsigned base, int width, int padc) {
    802005e2:	f022                	sd	s0,32(sp)
    802005e4:	ec26                	sd	s1,24(sp)
    802005e6:	e84a                	sd	s2,16(sp)
    802005e8:	f406                	sd	ra,40(sp)
    802005ea:	e44e                	sd	s3,8(sp)
    802005ec:	84aa                	mv	s1,a0
    802005ee:	892e                	mv	s2,a1
    802005f0:	fff7041b          	addiw	s0,a4,-1
    unsigned mod = do_div(result, base);
    802005f4:	2a01                	sext.w	s4,s4

    // first recursively print all preceding (more significant) digits
    if (num >= base) {
    802005f6:	03067e63          	bleu	a6,a2,80200632 <printnum+0x60>
    802005fa:	89be                	mv	s3,a5
        printnum(putch, putdat, result, base, width - 1, padc);
    } else {
        // print any needed pad characters before first digit
        while (-- width > 0)
    802005fc:	00805763          	blez	s0,8020060a <printnum+0x38>
    80200600:	347d                	addiw	s0,s0,-1
            putch(padc, putdat);
    80200602:	85ca                	mv	a1,s2
    80200604:	854e                	mv	a0,s3
    80200606:	9482                	jalr	s1
        while (-- width > 0)
    80200608:	fc65                	bnez	s0,80200600 <printnum+0x2e>
    }
    // then print this (the least significant) digit
    putch("0123456789abcdef"[mod], putdat);
    8020060a:	1a02                	slli	s4,s4,0x20
    8020060c:	020a5a13          	srli	s4,s4,0x20
    80200610:	00001797          	auipc	a5,0x1
    80200614:	bc878793          	addi	a5,a5,-1080 # 802011d8 <error_string+0x38>
    80200618:	9a3e                	add	s4,s4,a5
}
    8020061a:	7402                	ld	s0,32(sp)
    putch("0123456789abcdef"[mod], putdat);
    8020061c:	000a4503          	lbu	a0,0(s4)
}
    80200620:	70a2                	ld	ra,40(sp)
    80200622:	69a2                	ld	s3,8(sp)
    80200624:	6a02                	ld	s4,0(sp)
    putch("0123456789abcdef"[mod], putdat);
    80200626:	85ca                	mv	a1,s2
    80200628:	8326                	mv	t1,s1
}
    8020062a:	6942                	ld	s2,16(sp)
    8020062c:	64e2                	ld	s1,24(sp)
    8020062e:	6145                	addi	sp,sp,48
    putch("0123456789abcdef"[mod], putdat);
    80200630:	8302                	jr	t1
        printnum(putch, putdat, result, base, width - 1, padc);
    80200632:	03065633          	divu	a2,a2,a6
    80200636:	8722                	mv	a4,s0
    80200638:	f9bff0ef          	jal	ra,802005d2 <printnum>
    8020063c:	b7f9                	j	8020060a <printnum+0x38>

000000008020063e <vprintfmt>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want printfmt() instead.
 * */
void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap) {
    8020063e:	7119                	addi	sp,sp,-128
    80200640:	f4a6                	sd	s1,104(sp)
    80200642:	f0ca                	sd	s2,96(sp)
    80200644:	e8d2                	sd	s4,80(sp)
    80200646:	e4d6                	sd	s5,72(sp)
    80200648:	e0da                	sd	s6,64(sp)
    8020064a:	fc5e                	sd	s7,56(sp)
    8020064c:	f862                	sd	s8,48(sp)
    8020064e:	f06a                	sd	s10,32(sp)
    80200650:	fc86                	sd	ra,120(sp)
    80200652:	f8a2                	sd	s0,112(sp)
    80200654:	ecce                	sd	s3,88(sp)
    80200656:	f466                	sd	s9,40(sp)
    80200658:	ec6e                	sd	s11,24(sp)
    8020065a:	892a                	mv	s2,a0
    8020065c:	84ae                	mv	s1,a1
    8020065e:	8d32                	mv	s10,a2
    80200660:	8ab6                	mv	s5,a3
            putch(ch, putdat);
        }

        // Process a %-escape sequence
        char padc = ' ';
        width = precision = -1;
    80200662:	5b7d                	li	s6,-1
        lflag = altflag = 0;

    reswitch:
        switch (ch = *(unsigned char *)fmt ++) {
    80200664:	00001a17          	auipc	s4,0x1
    80200668:	9e0a0a13          	addi	s4,s4,-1568 # 80201044 <etext+0x5ee>
                for (width -= strnlen(p, precision); width > 0; width --) {
                    putch(padc, putdat);
                }
            }
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
                if (altflag && (ch < ' ' || ch > '~')) {
    8020066c:	05e00b93          	li	s7,94
            if (err > MAXERROR || (p = error_string[err]) == NULL) {
    80200670:	00001c17          	auipc	s8,0x1
    80200674:	b30c0c13          	addi	s8,s8,-1232 # 802011a0 <error_string>
        while ((ch = *(unsigned char *)fmt ++) != '%') {
    80200678:	000d4503          	lbu	a0,0(s10)
    8020067c:	02500793          	li	a5,37
    80200680:	001d0413          	addi	s0,s10,1
    80200684:	00f50e63          	beq	a0,a5,802006a0 <vprintfmt+0x62>
            if (ch == '\0') {
    80200688:	c521                	beqz	a0,802006d0 <vprintfmt+0x92>
        while ((ch = *(unsigned char *)fmt ++) != '%') {
    8020068a:	02500993          	li	s3,37
    8020068e:	a011                	j	80200692 <vprintfmt+0x54>
            if (ch == '\0') {
    80200690:	c121                	beqz	a0,802006d0 <vprintfmt+0x92>
            putch(ch, putdat);
    80200692:	85a6                	mv	a1,s1
        while ((ch = *(unsigned char *)fmt ++) != '%') {
    80200694:	0405                	addi	s0,s0,1
            putch(ch, putdat);
    80200696:	9902                	jalr	s2
        while ((ch = *(unsigned char *)fmt ++) != '%') {
    80200698:	fff44503          	lbu	a0,-1(s0)
    8020069c:	ff351ae3          	bne	a0,s3,80200690 <vprintfmt+0x52>
    802006a0:	00044603          	lbu	a2,0(s0)
        char padc = ' ';
    802006a4:	02000793          	li	a5,32
        lflag = altflag = 0;
    802006a8:	4981                	li	s3,0
    802006aa:	4801                	li	a6,0
        width = precision = -1;
    802006ac:	5cfd                	li	s9,-1
    802006ae:	5dfd                	li	s11,-1
        switch (ch = *(unsigned char *)fmt ++) {
    802006b0:	05500593          	li	a1,85
                if (ch < '0' || ch > '9') {
    802006b4:	4525                	li	a0,9
        switch (ch = *(unsigned char *)fmt ++) {
    802006b6:	fdd6069b          	addiw	a3,a2,-35
    802006ba:	0ff6f693          	andi	a3,a3,255
    802006be:	00140d13          	addi	s10,s0,1
    802006c2:	20d5e563          	bltu	a1,a3,802008cc <vprintfmt+0x28e>
    802006c6:	068a                	slli	a3,a3,0x2
    802006c8:	96d2                	add	a3,a3,s4
    802006ca:	4294                	lw	a3,0(a3)
    802006cc:	96d2                	add	a3,a3,s4
    802006ce:	8682                	jr	a3
            for (fmt --; fmt[-1] != '%'; fmt --)
                /* do nothing */;
            break;
        }
    }
}
    802006d0:	70e6                	ld	ra,120(sp)
    802006d2:	7446                	ld	s0,112(sp)
    802006d4:	74a6                	ld	s1,104(sp)
    802006d6:	7906                	ld	s2,96(sp)
    802006d8:	69e6                	ld	s3,88(sp)
    802006da:	6a46                	ld	s4,80(sp)
    802006dc:	6aa6                	ld	s5,72(sp)
    802006de:	6b06                	ld	s6,64(sp)
    802006e0:	7be2                	ld	s7,56(sp)
    802006e2:	7c42                	ld	s8,48(sp)
    802006e4:	7ca2                	ld	s9,40(sp)
    802006e6:	7d02                	ld	s10,32(sp)
    802006e8:	6de2                	ld	s11,24(sp)
    802006ea:	6109                	addi	sp,sp,128
    802006ec:	8082                	ret
    if (lflag >= 2) {
    802006ee:	4705                	li	a4,1
    802006f0:	008a8593          	addi	a1,s5,8
    802006f4:	01074463          	blt	a4,a6,802006fc <vprintfmt+0xbe>
    else if (lflag) {
    802006f8:	26080363          	beqz	a6,8020095e <vprintfmt+0x320>
        return va_arg(*ap, unsigned long);
    802006fc:	000ab603          	ld	a2,0(s5)
    80200700:	46c1                	li	a3,16
    80200702:	8aae                	mv	s5,a1
    80200704:	a06d                	j	802007ae <vprintfmt+0x170>
            goto reswitch;
    80200706:	00144603          	lbu	a2,1(s0)
            altflag = 1;
    8020070a:	4985                	li	s3,1
        switch (ch = *(unsigned char *)fmt ++) {
    8020070c:	846a                	mv	s0,s10
            goto reswitch;
    8020070e:	b765                	j	802006b6 <vprintfmt+0x78>
            putch(va_arg(ap, int), putdat);
    80200710:	000aa503          	lw	a0,0(s5)
    80200714:	85a6                	mv	a1,s1
    80200716:	0aa1                	addi	s5,s5,8
    80200718:	9902                	jalr	s2
            break;
    8020071a:	bfb9                	j	80200678 <vprintfmt+0x3a>
    if (lflag >= 2) {
    8020071c:	4705                	li	a4,1
    8020071e:	008a8993          	addi	s3,s5,8
    80200722:	01074463          	blt	a4,a6,8020072a <vprintfmt+0xec>
    else if (lflag) {
    80200726:	22080463          	beqz	a6,8020094e <vprintfmt+0x310>
        return va_arg(*ap, long);
    8020072a:	000ab403          	ld	s0,0(s5)
            if ((long long)num < 0) {
    8020072e:	24044463          	bltz	s0,80200976 <vprintfmt+0x338>
            num = getint(&ap, lflag);
    80200732:	8622                	mv	a2,s0
    80200734:	8ace                	mv	s5,s3
    80200736:	46a9                	li	a3,10
    80200738:	a89d                	j	802007ae <vprintfmt+0x170>
            err = va_arg(ap, int);
    8020073a:	000aa783          	lw	a5,0(s5)
            if (err > MAXERROR || (p = error_string[err]) == NULL) {
    8020073e:	4719                	li	a4,6
            err = va_arg(ap, int);
    80200740:	0aa1                	addi	s5,s5,8
            if (err < 0) {
    80200742:	41f7d69b          	sraiw	a3,a5,0x1f
    80200746:	8fb5                	xor	a5,a5,a3
    80200748:	40d786bb          	subw	a3,a5,a3
            if (err > MAXERROR || (p = error_string[err]) == NULL) {
    8020074c:	1ad74363          	blt	a4,a3,802008f2 <vprintfmt+0x2b4>
    80200750:	00369793          	slli	a5,a3,0x3
    80200754:	97e2                	add	a5,a5,s8
    80200756:	639c                	ld	a5,0(a5)
    80200758:	18078d63          	beqz	a5,802008f2 <vprintfmt+0x2b4>
                printfmt(putch, putdat, "%s", p);
    8020075c:	86be                	mv	a3,a5
    8020075e:	00001617          	auipc	a2,0x1
    80200762:	b2a60613          	addi	a2,a2,-1238 # 80201288 <error_string+0xe8>
    80200766:	85a6                	mv	a1,s1
    80200768:	854a                	mv	a0,s2
    8020076a:	240000ef          	jal	ra,802009aa <printfmt>
    8020076e:	b729                	j	80200678 <vprintfmt+0x3a>
            lflag ++;
    80200770:	00144603          	lbu	a2,1(s0)
    80200774:	2805                	addiw	a6,a6,1
        switch (ch = *(unsigned char *)fmt ++) {
    80200776:	846a                	mv	s0,s10
            goto reswitch;
    80200778:	bf3d                	j	802006b6 <vprintfmt+0x78>
    if (lflag >= 2) {
    8020077a:	4705                	li	a4,1
    8020077c:	008a8593          	addi	a1,s5,8
    80200780:	01074463          	blt	a4,a6,80200788 <vprintfmt+0x14a>
    else if (lflag) {
    80200784:	1e080263          	beqz	a6,80200968 <vprintfmt+0x32a>
        return va_arg(*ap, unsigned long);
    80200788:	000ab603          	ld	a2,0(s5)
    8020078c:	46a1                	li	a3,8
    8020078e:	8aae                	mv	s5,a1
    80200790:	a839                	j	802007ae <vprintfmt+0x170>
            putch('0', putdat);
    80200792:	03000513          	li	a0,48
    80200796:	85a6                	mv	a1,s1
    80200798:	e03e                	sd	a5,0(sp)
    8020079a:	9902                	jalr	s2
            putch('x', putdat);
    8020079c:	85a6                	mv	a1,s1
    8020079e:	07800513          	li	a0,120
    802007a2:	9902                	jalr	s2
            num = (unsigned long long)va_arg(ap, void *);
    802007a4:	0aa1                	addi	s5,s5,8
    802007a6:	ff8ab603          	ld	a2,-8(s5)
            goto number;
    802007aa:	6782                	ld	a5,0(sp)
    802007ac:	46c1                	li	a3,16
            printnum(putch, putdat, num, base, width, padc);
    802007ae:	876e                	mv	a4,s11
    802007b0:	85a6                	mv	a1,s1
    802007b2:	854a                	mv	a0,s2
    802007b4:	e1fff0ef          	jal	ra,802005d2 <printnum>
            break;
    802007b8:	b5c1                	j	80200678 <vprintfmt+0x3a>
            if ((p = va_arg(ap, char *)) == NULL) {
    802007ba:	000ab603          	ld	a2,0(s5)
    802007be:	0aa1                	addi	s5,s5,8
    802007c0:	1c060663          	beqz	a2,8020098c <vprintfmt+0x34e>
            if (width > 0 && padc != '-') {
    802007c4:	00160413          	addi	s0,a2,1
    802007c8:	17b05c63          	blez	s11,80200940 <vprintfmt+0x302>
    802007cc:	02d00593          	li	a1,45
    802007d0:	14b79263          	bne	a5,a1,80200914 <vprintfmt+0x2d6>
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
    802007d4:	00064783          	lbu	a5,0(a2)
    802007d8:	0007851b          	sext.w	a0,a5
    802007dc:	c905                	beqz	a0,8020080c <vprintfmt+0x1ce>
    802007de:	000cc563          	bltz	s9,802007e8 <vprintfmt+0x1aa>
    802007e2:	3cfd                	addiw	s9,s9,-1
    802007e4:	036c8263          	beq	s9,s6,80200808 <vprintfmt+0x1ca>
                    putch('?', putdat);
    802007e8:	85a6                	mv	a1,s1
                if (altflag && (ch < ' ' || ch > '~')) {
    802007ea:	18098463          	beqz	s3,80200972 <vprintfmt+0x334>
    802007ee:	3781                	addiw	a5,a5,-32
    802007f0:	18fbf163          	bleu	a5,s7,80200972 <vprintfmt+0x334>
                    putch('?', putdat);
    802007f4:	03f00513          	li	a0,63
    802007f8:	9902                	jalr	s2
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
    802007fa:	0405                	addi	s0,s0,1
    802007fc:	fff44783          	lbu	a5,-1(s0)
    80200800:	3dfd                	addiw	s11,s11,-1
    80200802:	0007851b          	sext.w	a0,a5
    80200806:	fd61                	bnez	a0,802007de <vprintfmt+0x1a0>
            for (; width > 0; width --) {
    80200808:	e7b058e3          	blez	s11,80200678 <vprintfmt+0x3a>
    8020080c:	3dfd                	addiw	s11,s11,-1
                putch(' ', putdat);
    8020080e:	85a6                	mv	a1,s1
    80200810:	02000513          	li	a0,32
    80200814:	9902                	jalr	s2
            for (; width > 0; width --) {
    80200816:	e60d81e3          	beqz	s11,80200678 <vprintfmt+0x3a>
    8020081a:	3dfd                	addiw	s11,s11,-1
                putch(' ', putdat);
    8020081c:	85a6                	mv	a1,s1
    8020081e:	02000513          	li	a0,32
    80200822:	9902                	jalr	s2
            for (; width > 0; width --) {
    80200824:	fe0d94e3          	bnez	s11,8020080c <vprintfmt+0x1ce>
    80200828:	bd81                	j	80200678 <vprintfmt+0x3a>
    if (lflag >= 2) {
    8020082a:	4705                	li	a4,1
    8020082c:	008a8593          	addi	a1,s5,8
    80200830:	01074463          	blt	a4,a6,80200838 <vprintfmt+0x1fa>
    else if (lflag) {
    80200834:	12080063          	beqz	a6,80200954 <vprintfmt+0x316>
        return va_arg(*ap, unsigned long);
    80200838:	000ab603          	ld	a2,0(s5)
    8020083c:	46a9                	li	a3,10
    8020083e:	8aae                	mv	s5,a1
    80200840:	b7bd                	j	802007ae <vprintfmt+0x170>
    80200842:	00144603          	lbu	a2,1(s0)
            padc = '-';
    80200846:	02d00793          	li	a5,45
        switch (ch = *(unsigned char *)fmt ++) {
    8020084a:	846a                	mv	s0,s10
    8020084c:	b5ad                	j	802006b6 <vprintfmt+0x78>
            putch(ch, putdat);
    8020084e:	85a6                	mv	a1,s1
    80200850:	02500513          	li	a0,37
    80200854:	9902                	jalr	s2
            break;
    80200856:	b50d                	j	80200678 <vprintfmt+0x3a>
            precision = va_arg(ap, int);
    80200858:	000aac83          	lw	s9,0(s5)
            goto process_precision;
    8020085c:	00144603          	lbu	a2,1(s0)
            precision = va_arg(ap, int);
    80200860:	0aa1                	addi	s5,s5,8
        switch (ch = *(unsigned char *)fmt ++) {
    80200862:	846a                	mv	s0,s10
            if (width < 0)
    80200864:	e40dd9e3          	bgez	s11,802006b6 <vprintfmt+0x78>
                width = precision, precision = -1;
    80200868:	8de6                	mv	s11,s9
    8020086a:	5cfd                	li	s9,-1
    8020086c:	b5a9                	j	802006b6 <vprintfmt+0x78>
            goto reswitch;
    8020086e:	00144603          	lbu	a2,1(s0)
            padc = '0';
    80200872:	03000793          	li	a5,48
        switch (ch = *(unsigned char *)fmt ++) {
    80200876:	846a                	mv	s0,s10
            goto reswitch;
    80200878:	bd3d                	j	802006b6 <vprintfmt+0x78>
                precision = precision * 10 + ch - '0';
    8020087a:	fd060c9b          	addiw	s9,a2,-48
                ch = *fmt;
    8020087e:	00144603          	lbu	a2,1(s0)
        switch (ch = *(unsigned char *)fmt ++) {
    80200882:	846a                	mv	s0,s10
                if (ch < '0' || ch > '9') {
    80200884:	fd06069b          	addiw	a3,a2,-48
                ch = *fmt;
    80200888:	0006089b          	sext.w	a7,a2
                if (ch < '0' || ch > '9') {
    8020088c:	fcd56ce3          	bltu	a0,a3,80200864 <vprintfmt+0x226>
            for (precision = 0; ; ++ fmt) {
    80200890:	0405                	addi	s0,s0,1
                precision = precision * 10 + ch - '0';
    80200892:	002c969b          	slliw	a3,s9,0x2
                ch = *fmt;
    80200896:	00044603          	lbu	a2,0(s0)
                precision = precision * 10 + ch - '0';
    8020089a:	0196873b          	addw	a4,a3,s9
    8020089e:	0017171b          	slliw	a4,a4,0x1
    802008a2:	0117073b          	addw	a4,a4,a7
                if (ch < '0' || ch > '9') {
    802008a6:	fd06069b          	addiw	a3,a2,-48
                precision = precision * 10 + ch - '0';
    802008aa:	fd070c9b          	addiw	s9,a4,-48
                ch = *fmt;
    802008ae:	0006089b          	sext.w	a7,a2
                if (ch < '0' || ch > '9') {
    802008b2:	fcd57fe3          	bleu	a3,a0,80200890 <vprintfmt+0x252>
    802008b6:	b77d                	j	80200864 <vprintfmt+0x226>
            if (width < 0)
    802008b8:	fffdc693          	not	a3,s11
    802008bc:	96fd                	srai	a3,a3,0x3f
    802008be:	00ddfdb3          	and	s11,s11,a3
    802008c2:	00144603          	lbu	a2,1(s0)
    802008c6:	2d81                	sext.w	s11,s11
        switch (ch = *(unsigned char *)fmt ++) {
    802008c8:	846a                	mv	s0,s10
    802008ca:	b3f5                	j	802006b6 <vprintfmt+0x78>
            putch('%', putdat);
    802008cc:	85a6                	mv	a1,s1
    802008ce:	02500513          	li	a0,37
    802008d2:	9902                	jalr	s2
            for (fmt --; fmt[-1] != '%'; fmt --)
    802008d4:	fff44703          	lbu	a4,-1(s0)
    802008d8:	02500793          	li	a5,37
    802008dc:	8d22                	mv	s10,s0
    802008de:	d8f70de3          	beq	a4,a5,80200678 <vprintfmt+0x3a>
    802008e2:	02500713          	li	a4,37
    802008e6:	1d7d                	addi	s10,s10,-1
    802008e8:	fffd4783          	lbu	a5,-1(s10)
    802008ec:	fee79de3          	bne	a5,a4,802008e6 <vprintfmt+0x2a8>
    802008f0:	b361                	j	80200678 <vprintfmt+0x3a>
                printfmt(putch, putdat, "error %d", err);
    802008f2:	00001617          	auipc	a2,0x1
    802008f6:	98660613          	addi	a2,a2,-1658 # 80201278 <error_string+0xd8>
    802008fa:	85a6                	mv	a1,s1
    802008fc:	854a                	mv	a0,s2
    802008fe:	0ac000ef          	jal	ra,802009aa <printfmt>
    80200902:	bb9d                	j	80200678 <vprintfmt+0x3a>
                p = "(null)";
    80200904:	00001617          	auipc	a2,0x1
    80200908:	96c60613          	addi	a2,a2,-1684 # 80201270 <error_string+0xd0>
            if (width > 0 && padc != '-') {
    8020090c:	00001417          	auipc	s0,0x1
    80200910:	96540413          	addi	s0,s0,-1691 # 80201271 <error_string+0xd1>
                for (width -= strnlen(p, precision); width > 0; width --) {
    80200914:	8532                	mv	a0,a2
    80200916:	85e6                	mv	a1,s9
    80200918:	e032                	sd	a2,0(sp)
    8020091a:	e43e                	sd	a5,8(sp)
    8020091c:	102000ef          	jal	ra,80200a1e <strnlen>
    80200920:	40ad8dbb          	subw	s11,s11,a0
    80200924:	6602                	ld	a2,0(sp)
    80200926:	01b05d63          	blez	s11,80200940 <vprintfmt+0x302>
    8020092a:	67a2                	ld	a5,8(sp)
    8020092c:	2781                	sext.w	a5,a5
    8020092e:	e43e                	sd	a5,8(sp)
                    putch(padc, putdat);
    80200930:	6522                	ld	a0,8(sp)
    80200932:	85a6                	mv	a1,s1
    80200934:	e032                	sd	a2,0(sp)
                for (width -= strnlen(p, precision); width > 0; width --) {
    80200936:	3dfd                	addiw	s11,s11,-1
                    putch(padc, putdat);
    80200938:	9902                	jalr	s2
                for (width -= strnlen(p, precision); width > 0; width --) {
    8020093a:	6602                	ld	a2,0(sp)
    8020093c:	fe0d9ae3          	bnez	s11,80200930 <vprintfmt+0x2f2>
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
    80200940:	00064783          	lbu	a5,0(a2)
    80200944:	0007851b          	sext.w	a0,a5
    80200948:	e8051be3          	bnez	a0,802007de <vprintfmt+0x1a0>
    8020094c:	b335                	j	80200678 <vprintfmt+0x3a>
        return va_arg(*ap, int);
    8020094e:	000aa403          	lw	s0,0(s5)
    80200952:	bbf1                	j	8020072e <vprintfmt+0xf0>
        return va_arg(*ap, unsigned int);
    80200954:	000ae603          	lwu	a2,0(s5)
    80200958:	46a9                	li	a3,10
    8020095a:	8aae                	mv	s5,a1
    8020095c:	bd89                	j	802007ae <vprintfmt+0x170>
    8020095e:	000ae603          	lwu	a2,0(s5)
    80200962:	46c1                	li	a3,16
    80200964:	8aae                	mv	s5,a1
    80200966:	b5a1                	j	802007ae <vprintfmt+0x170>
    80200968:	000ae603          	lwu	a2,0(s5)
    8020096c:	46a1                	li	a3,8
    8020096e:	8aae                	mv	s5,a1
    80200970:	bd3d                	j	802007ae <vprintfmt+0x170>
                    putch(ch, putdat);
    80200972:	9902                	jalr	s2
    80200974:	b559                	j	802007fa <vprintfmt+0x1bc>
                putch('-', putdat);
    80200976:	85a6                	mv	a1,s1
    80200978:	02d00513          	li	a0,45
    8020097c:	e03e                	sd	a5,0(sp)
    8020097e:	9902                	jalr	s2
                num = -(long long)num;
    80200980:	8ace                	mv	s5,s3
    80200982:	40800633          	neg	a2,s0
    80200986:	46a9                	li	a3,10
    80200988:	6782                	ld	a5,0(sp)
    8020098a:	b515                	j	802007ae <vprintfmt+0x170>
            if (width > 0 && padc != '-') {
    8020098c:	01b05663          	blez	s11,80200998 <vprintfmt+0x35a>
    80200990:	02d00693          	li	a3,45
    80200994:	f6d798e3          	bne	a5,a3,80200904 <vprintfmt+0x2c6>
    80200998:	00001417          	auipc	s0,0x1
    8020099c:	8d940413          	addi	s0,s0,-1831 # 80201271 <error_string+0xd1>
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
    802009a0:	02800513          	li	a0,40
    802009a4:	02800793          	li	a5,40
    802009a8:	bd1d                	j	802007de <vprintfmt+0x1a0>

00000000802009aa <printfmt>:
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...) {
    802009aa:	715d                	addi	sp,sp,-80
    va_start(ap, fmt);
    802009ac:	02810313          	addi	t1,sp,40
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...) {
    802009b0:	f436                	sd	a3,40(sp)
    vprintfmt(putch, putdat, fmt, ap);
    802009b2:	869a                	mv	a3,t1
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...) {
    802009b4:	ec06                	sd	ra,24(sp)
    802009b6:	f83a                	sd	a4,48(sp)
    802009b8:	fc3e                	sd	a5,56(sp)
    802009ba:	e0c2                	sd	a6,64(sp)
    802009bc:	e4c6                	sd	a7,72(sp)
    va_start(ap, fmt);
    802009be:	e41a                	sd	t1,8(sp)
    vprintfmt(putch, putdat, fmt, ap);
    802009c0:	c7fff0ef          	jal	ra,8020063e <vprintfmt>
}
    802009c4:	60e2                	ld	ra,24(sp)
    802009c6:	6161                	addi	sp,sp,80
    802009c8:	8082                	ret

00000000802009ca <sbi_console_putchar>:

int sbi_console_getchar(void) {
    return sbi_call(SBI_CONSOLE_GETCHAR, 0, 0, 0);
}
void sbi_console_putchar(unsigned char ch) {
    sbi_call(SBI_CONSOLE_PUTCHAR, ch, 0, 0);
    802009ca:	00003797          	auipc	a5,0x3
    802009ce:	63678793          	addi	a5,a5,1590 # 80204000 <bootstacktop>
    __asm__ volatile (
    802009d2:	6398                	ld	a4,0(a5)
    802009d4:	4781                	li	a5,0
    802009d6:	88ba                	mv	a7,a4
    802009d8:	852a                	mv	a0,a0
    802009da:	85be                	mv	a1,a5
    802009dc:	863e                	mv	a2,a5
    802009de:	00000073          	ecall
    802009e2:	87aa                	mv	a5,a0
}
    802009e4:	8082                	ret

00000000802009e6 <sbi_set_timer>:

void sbi_set_timer(unsigned long long stime_value) {
    sbi_call(SBI_SET_TIMER, stime_value, 0, 0);
    802009e6:	00003797          	auipc	a5,0x3
    802009ea:	63278793          	addi	a5,a5,1586 # 80204018 <SBI_SET_TIMER>
    __asm__ volatile (
    802009ee:	6398                	ld	a4,0(a5)
    802009f0:	4781                	li	a5,0
    802009f2:	88ba                	mv	a7,a4
    802009f4:	852a                	mv	a0,a0
    802009f6:	85be                	mv	a1,a5
    802009f8:	863e                	mv	a2,a5
    802009fa:	00000073          	ecall
    802009fe:	87aa                	mv	a5,a0
}
    80200a00:	8082                	ret

0000000080200a02 <sbi_shutdown>:


void sbi_shutdown(void)
{
    sbi_call(SBI_SHUTDOWN,0,0,0);
    80200a02:	00003797          	auipc	a5,0x3
    80200a06:	60678793          	addi	a5,a5,1542 # 80204008 <SBI_SHUTDOWN>
    __asm__ volatile (
    80200a0a:	6398                	ld	a4,0(a5)
    80200a0c:	4781                	li	a5,0
    80200a0e:	88ba                	mv	a7,a4
    80200a10:	853e                	mv	a0,a5
    80200a12:	85be                	mv	a1,a5
    80200a14:	863e                	mv	a2,a5
    80200a16:	00000073          	ecall
    80200a1a:	87aa                	mv	a5,a0
    80200a1c:	8082                	ret

0000000080200a1e <strnlen>:
 * pointed by @s.
 * */
size_t
strnlen(const char *s, size_t len) {
    size_t cnt = 0;
    while (cnt < len && *s ++ != '\0') {
    80200a1e:	c185                	beqz	a1,80200a3e <strnlen+0x20>
    80200a20:	00054783          	lbu	a5,0(a0)
    80200a24:	cf89                	beqz	a5,80200a3e <strnlen+0x20>
    size_t cnt = 0;
    80200a26:	4781                	li	a5,0
    80200a28:	a021                	j	80200a30 <strnlen+0x12>
    while (cnt < len && *s ++ != '\0') {
    80200a2a:	00074703          	lbu	a4,0(a4)
    80200a2e:	c711                	beqz	a4,80200a3a <strnlen+0x1c>
        cnt ++;
    80200a30:	0785                	addi	a5,a5,1
    while (cnt < len && *s ++ != '\0') {
    80200a32:	00f50733          	add	a4,a0,a5
    80200a36:	fef59ae3          	bne	a1,a5,80200a2a <strnlen+0xc>
    }
    return cnt;
}
    80200a3a:	853e                	mv	a0,a5
    80200a3c:	8082                	ret
    size_t cnt = 0;
    80200a3e:	4781                	li	a5,0
}
    80200a40:	853e                	mv	a0,a5
    80200a42:	8082                	ret

0000000080200a44 <memset>:
memset(void *s, char c, size_t n) {
#ifdef __HAVE_ARCH_MEMSET
    return __memset(s, c, n);
#else
    char *p = s;
    while (n -- > 0) {
    80200a44:	ca01                	beqz	a2,80200a54 <memset+0x10>
    80200a46:	962a                	add	a2,a2,a0
    char *p = s;
    80200a48:	87aa                	mv	a5,a0
        *p ++ = c;
    80200a4a:	0785                	addi	a5,a5,1
    80200a4c:	feb78fa3          	sb	a1,-1(a5)
    while (n -- > 0) {
    80200a50:	fec79de3          	bne	a5,a2,80200a4a <memset+0x6>
    }
    return s;
#endif /* __HAVE_ARCH_MEMSET */
}
    80200a54:	8082                	ret
